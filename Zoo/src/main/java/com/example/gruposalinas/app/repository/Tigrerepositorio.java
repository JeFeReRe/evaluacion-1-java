package com.example.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gruposalinas.app.entity.Tigre;

public interface Tigrerepositorio extends JpaRepository<Tigre, Long> {

}
