package com.example.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gruposalinas.app.entity.Leon;
import com.example.gruposalinas.app.services.LeonServicio;

@RestController
@RequestMapping("api/Leon")
public class LeonController {
@Autowired
private LeonServicio leonservicio;
@PostMapping
public ResponseEntity<?> create(@RequestBody Leon leon)
{
	return ResponseEntity.status(HttpStatus.CREATED).body(leonservicio.save(leon));
}
@GetMapping("/{id}")
public ResponseEntity<?> read(@PathVariable(value="id") Long id){
	Optional<Leon> oUsuario = leonservicio.findById(id);
	if(!oUsuario.isPresent())
	{
		return ResponseEntity.notFound().build();
	}
	return ResponseEntity.ok(oUsuario);
}
@PutMapping("/{id}")
public ResponseEntity<?> update(@RequestBody Leon usuarioDetalles,@PathVariable(value="id") Long id )
{
Optional<Leon> oUsuario = leonservicio.findById(id);
if(!oUsuario.isPresent())
{
	return ResponseEntity.notFound().build();
}
oUsuario.get().setEspecie(usuarioDetalles.getEspecie());
oUsuario.get().setAnimal(usuarioDetalles.getAnimal());
oUsuario.get().setEdad(usuarioDetalles.getEdad());
oUsuario.get().setTamaño(usuarioDetalles.getTamaño());
oUsuario.get().setSexo(usuarioDetalles.getSexo());




return ResponseEntity.status(HttpStatus.CREATED).body(leonservicio.save(oUsuario.get()));
}
@DeleteMapping("/{id}")
public ResponseEntity<?> delete(@PathVariable(value="id") Long id){
	Optional<Leon> oUsuario = leonservicio.findById(id);
	if(!oUsuario.isPresent())
	{
		return ResponseEntity.notFound().build();
	}
	leonservicio.deleteById(id);
return ResponseEntity.ok().build();
}
@GetMapping
public ResponseEntity<?> readAll(){

	return ResponseEntity.status(HttpStatus.ACCEPTED).body(leonservicio.findAll());
}
@GetMapping("/all")
public String readAllone(){
	int T = 0;
	for(int i=0;i<=1000;i++)
	{
		Optional<Leon> oUsuario = leonservicio.findById(Long.valueOf(i));
		if(oUsuario.isPresent())
		{
			T++;
			
		}
	}

	return "El total son:"+ String.valueOf(T);
}
	
	
}
