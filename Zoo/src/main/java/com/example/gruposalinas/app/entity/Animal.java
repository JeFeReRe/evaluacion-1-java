package com.example.gruposalinas.app.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Animal")
public class Animal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;
	private Long IdAnimal;
	@Column(length = 50)
	private String Especies;
	private Long Cantidad;
	@Column(length = 50)
	private String Jaulas;
	@Column(length = 50)
	private String Comida;
	private Boolean Existencia;
	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public Long getIdAnimal() {
		return IdAnimal;
	}
	public void setIdAnimal(Long idAnimal) {
		IdAnimal = idAnimal;
	}
	public String getEspecies() {
		return Especies;
	}
	public void setEspecies(String especies) {
		Especies = especies;
	}
	public Long getCantidad() {
		return Cantidad;
	}
	public void setCantidad(Long cantidad) {
		Cantidad = cantidad;
	}
	public String getJaulas() {
		return Jaulas;
	}
	public void setJaulas(String jaulas) {
		Jaulas = jaulas;
	}
	public String getComida() {
		return Comida;
	}
	public void setComida(String comida) {
		Comida = comida;
	}
	public Boolean getExistencia() {
		return Existencia;
	}
	public void setExistencia(Boolean existencia) {
		Existencia = existencia;
	}

	
	

}
