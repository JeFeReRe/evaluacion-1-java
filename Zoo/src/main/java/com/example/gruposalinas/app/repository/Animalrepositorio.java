package com.example.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.gruposalinas.app.entity.Animal;

@Repository
public interface Animalrepositorio extends JpaRepository<Animal, Long> {	
}
