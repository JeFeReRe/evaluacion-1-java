package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.gruposalinas.app.entity.Puma;

@Service
public interface PumaServicio {
	public Iterable<Puma> findAll();
	public Page<Puma> findAll(Pageable pageable);
	public Optional<Puma> findById(Long id);
	public Puma save (Puma puma);
	public void deleteById(Long id);

}
