package com.example.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gruposalinas.app.entity.Puma;

public interface Pumarepositorio extends JpaRepository<Puma, Long> {

}
