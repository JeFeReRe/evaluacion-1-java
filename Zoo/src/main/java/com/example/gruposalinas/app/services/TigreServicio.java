package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.gruposalinas.app.entity.Tigre;

@Service
public interface TigreServicio {
	public Iterable<Tigre> findAll();
	public Page<Tigre> findAll(Pageable pageable);
	public Optional<Tigre> findById(Long id);
	public Tigre save (Tigre eigre);
	public void deleteById(Long id);

}
