package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.gruposalinas.app.entity.Animal;
import com.example.gruposalinas.app.repository.Animalrepositorio;

@Service
public class AnimalServicioImplements implements AnimalServicio {
	
	@Autowired
	private Animalrepositorio animalrepositorio;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Animal> findAll() {
		// TODO Auto-generated method stub
		return animalrepositorio.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Animal> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return animalrepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Animal> findById(Long id) {
		// TODO Auto-generated method stub
		return animalrepositorio.findById(id);
	}

	@Override
	@Transactional
	public Animal save(Animal animal) {
		// TODO Auto-generated method stub
		return animalrepositorio.save(animal);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		animalrepositorio.deleteById(id);
		
	}

}
