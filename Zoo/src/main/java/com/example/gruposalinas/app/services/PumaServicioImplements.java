package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.gruposalinas.app.entity.Puma;
import com.example.gruposalinas.app.repository.Pumarepositorio;

@Service
public class PumaServicioImplements implements PumaServicio {
	
	@Autowired
	private Pumarepositorio pumarepositorio;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Puma> findAll() {
		// TODO Auto-generated method stub
		return pumarepositorio.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Puma> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return pumarepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Puma> findById(Long id) {
		// TODO Auto-generated method stub
		return pumarepositorio.findById(id);
	}

	@Override
	@Transactional
	public Puma save(Puma puma) {
		// TODO Auto-generated method stub
		return pumarepositorio.save(puma);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		pumarepositorio.deleteById(id);
	}

}
