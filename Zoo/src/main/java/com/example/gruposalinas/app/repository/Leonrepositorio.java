package com.example.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gruposalinas.app.entity.Leon;

public interface Leonrepositorio extends JpaRepository<Leon, Long> {

}
