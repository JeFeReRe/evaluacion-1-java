package com.example.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gruposalinas.app.entity.Animal;
import com.example.gruposalinas.app.services.AnimalServicio;


@RestController
@RequestMapping("api/Animal")
public class AnimalController {
	
	
	@Autowired
	private AnimalServicio animalservicio;
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Animal animal)
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(animalservicio.save(animal));
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value="id") Long id){
		Optional<Animal> oAnimal = animalservicio.findById(id);
		if(!oAnimal.isPresent())
		{
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oAnimal);
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Animal AnimalDetalles,@PathVariable(value="id") Long id )
	{
	Optional<Animal> oAnimal = animalservicio.findById(id);
	if(!oAnimal.isPresent())
	{
		return ResponseEntity.notFound().build();
	}
	oAnimal.get().setIdAnimal(AnimalDetalles.getIdAnimal());
	oAnimal.get().setEspecies(AnimalDetalles.getEspecies());
	oAnimal.get().setCantidad(AnimalDetalles.getCantidad());
	oAnimal.get().setJaulas(AnimalDetalles.getJaulas());
	oAnimal.get().setComida(AnimalDetalles.getComida());
	oAnimal.get().setExistencia(AnimalDetalles.getExistencia());
	
	
	
	return ResponseEntity.status(HttpStatus.CREATED).body(animalservicio.save(oAnimal.get()));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value="id") Long id){
		Optional<Animal> oAnimal = animalservicio.findById(id);
		if(!oAnimal.isPresent())
		{
			return ResponseEntity.notFound().build();
		}
		animalservicio.deleteById(id);
	return ResponseEntity.ok().build();
	}
	@GetMapping
	public ResponseEntity<?> readAll(){

		return ResponseEntity.status(HttpStatus.ACCEPTED).body(animalservicio.findAll());
	}
	@GetMapping("/all")
	public String readAllone(){
		int T = 0;
		for(int i=0;i<=1000;i++)
		{
			Optional<Animal> oAnimal = animalservicio.findById(Long.valueOf(i));
			if(oAnimal.isPresent())
			{
				T++;
				
			}
		}

		return "El total son:"+ String.valueOf(T);
	}

}
