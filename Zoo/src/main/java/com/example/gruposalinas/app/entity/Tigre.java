package com.example.gruposalinas.app.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "Tigre")
public class Tigre implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long IdAnimal;
	@Column(length = 50)
	private String Especie;
	@Column(length = 50)
	private String Animal;
	private Long Edad;
	@Column(length = 50)
	private String Tamaño;
	private Boolean Sexo;
	public Long getIdAnimal() {
		return IdAnimal;
	}
	public void setIdAnimal(Long idAnimal) {
		IdAnimal = idAnimal;
	}
	public String getEspecie() {
		return Especie;
	}
	public void setEspecie(String especie) {
		Especie = especie;
	}
	public String getAnimal() {
		return Animal;
	}
	public void setAnimal(String animal) {
		Animal = animal;
	}
	public Long getEdad() {
		return Edad;
	}
	public void setEdad(Long edad) {
		Edad = edad;
	}
	public String getTamaño() {
		return Tamaño;
	}
	public void setTamaño(String tamaño) {
		Tamaño = tamaño;
	}
	public Boolean getSexo() {
		return Sexo;
	}
	public void setSexo(Boolean sexo) {
		Sexo = sexo;
	}

}
