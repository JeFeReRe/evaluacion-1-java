package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.gruposalinas.app.entity.Leon;
import com.example.gruposalinas.app.repository.Leonrepositorio;

@Service
public class LeonServicioImplements implements LeonServicio {

	@Autowired
	private Leonrepositorio leonrepositorio;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Leon> findAll() {
		// TODO Auto-generated method stub
		return leonrepositorio.findAll() ;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Leon> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return leonrepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Leon> findById(Long id) {
		// TODO Auto-generated method stub
		return leonrepositorio.findById(id);
	}

	@Override
	@Transactional
	public Leon save(Leon leon) {
		// TODO Auto-generated method stub
		return leonrepositorio.save(leon);
	}
	@Transactional
	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		leonrepositorio.deleteById(id);
	}
	

}
