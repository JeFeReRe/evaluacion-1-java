package com.example.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.gruposalinas.app.entity.Leon;

@Service
public interface LeonServicio {
	public Iterable<Leon> findAll();
	public Page<Leon> findAll(Pageable pageable);
	public Optional<Leon> findById(Long id);
	public Leon save (Leon leon);
	public void deleteById(Long id);
}
